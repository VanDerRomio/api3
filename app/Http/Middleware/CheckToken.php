<?php

namespace App\Http\Middleware;

use App\OAuth_client;
use Closure;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $error_400 = ['errors' => [
            'status' => 400,
            'message' => 'token nie odpowiada rzeczywistości'
        ]];


        if($request->hasHeader('token') || $request->has('token')){
            $token = $request->header('token') ?? $request->get('token');

            //check corect token
            $tmp_token = explode('.', $token);

            if(is_array($tmp_token) && count($tmp_token) == 3){
                $header = $tmp_token[0];
                try{
                    $payload = json_decode(base64_decode($tmp_token[1]), true);
                }
                catch(\Exception $exception){
                    return response()->json($error_400, 400);
                }
                $signature = $tmp_token[2];

                /*'iss' => 'Master Romio',
                'sub' => 'auth',
                'id'=>111*/

                if($data = OAuth_client::find($payload['id'])) {
                    $newSignature = hash_hmac(env('APP_ALG'), base64_encode(json_encode([$data->id, $data->secret, $data->user_id])), env('APP_KEY'));
                    if($signature === $newSignature){
                        //all ok - go next
                        return $next($request);
                    }
                    else{
                        return response()->json($error_400, 400);
                    }
                }
                else{
                    return response()->json($error_400, 400);
                }
            }
            else{
                return response()->json($error_400, 400);
            }
        }
        else{
            return response()->json($error_400, 400);
        }

        return $next($request);
    }
}
