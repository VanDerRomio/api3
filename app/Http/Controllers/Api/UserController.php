<?php

namespace App\Http\Controllers\Api;

use App\User;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $select = $request->has('select')?explode(',', request('select')):'*';
            $sort = $request->has('sort')?request('sort'):'id';
            $orderby = $request->has('orderby')?request('orderby'):'asc';

            if(is_array($select)){
                foreach($select as $key => $column){
                    if(!Schema::hasColumn('users', $column))
                        return response()->json([
                            'errors' => [
                                'status' => 400,
                                'message' => 'Użytkownik nie ma atrybutu: ' . $column,
                            ]
                        ], 400);
                }
            }
            if(is_string($sort) && !empty($sort)){
                if(!Schema::hasColumn('users', $sort))
                    return response()->json([
                        'errors' => [
                            'status' => 400,
                            'message' => 'użytkownik nie ma atrybutu: ' . $sort,
                        ]
                    ], 400);
            }
            if(is_string($orderby)){
                if(!in_array($orderby, ['asc', 'desc']))
                    return response()->json([
                        'errors' => [
                            'status' => 400,
                            'message' => 'Sortowanie powinno być "asc" lub "desk"',
                        ]
                    ], 400);
            }

            $data = DB::table('users')->select($select)->orderBy($sort, $orderby)->paginate(3);
        }
        catch(\Exception $exception){
            return response()->json([
                'errors' => [
                    'status' => 400,
                    'message' => 'Bad request',
                ]
            ], 200);
        }
        return response()->json([$data], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'lastName' => ['required', 'string', 'max:255'],
                'role' => ['required', 'string', 'max:255'],
                'age' => ['required', 'integer', 'max:130'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:6', 'confirmed'],
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => [
                        'status' => 400,
                        'message' => 'Użytkownik nie został utworzony',
                        'details' => $validator->errors()
                    ]
                ], 400);
            }

            if($data = User::create([
                'name' => request('name'),
                'lastName' => request('lastName'),
                'role' => request('role'),
                'age' => request('age'),
                'email' => request('email'),
                'password' => Hash::make(request('password')),
            ])) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Użytkownik został pomyślnie utworzony',
                ], 200);
            }
            else{
                return response()->json([
                    'errors' => [
                        'status' => 400,
                        'message' => 'Użytkownik nie został utworzony',
                    ]
                ], 400);
            }
        }
        catch(\Exception $exception){
            return response()->json([
                'errors' => [
                    'status' => 400,
                    'message' => 'Użytkownik nie został utworzony',
                ]
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show(Request $request, $id = null)
    {
        if(!is_numeric($id)){
            return response()->json([
                'errors' => [
                    'status' => 400,
                    'message' => 'Identyfikator użytkownika musi być liczbą',
                ]
            ], 400);
        }
        try {
            $select = ($request->has('select') && !empty(request('select')))?explode(',', request('select')):'*';

            if(is_array($select) && !empty($select) && $select[0] != '*'){
                foreach($select as $key => $column){
                    if(!Schema::hasColumn('users', $column))
                        return response()->json([
                            'errors' => [
                                'status' => 400,
                                'message' => 'Użytkownik nie ma atrybutu: ' . $column,
                            ]
                        ], 400);
                }
            }
            $data = DB::table('users')->where('id', $id)->select($select)->first();
            if(!$data){
                return response()->json([
                    'errors' => [
                        'status' => 404,
                        'message' => 'Nie ma użytkownika o danym ID',
                    ]
                ], 404);
            }
        }
        catch(\Exception $exception){
            return response()->json([
                'errors' => [
                    'status' => 400,
                    'message' => 'Bad request',
                ]
            ], 200);
        }
        return response()->json(['data' => $data], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!is_numeric($id)){
            return response()->json([
                'errors' => [
                    'status' => 400,
                    'message' => 'Identyfikator użytkownika musi być liczbą',
                ]
            ], 400);
        }
        try{
            $validator = Validator::make($request->all(), [
                'name' => ['string', 'max:255'],
                'lastName' => ['string', 'max:255'],
                'role' => ['string', 'max:255'],
                'age' => ['integer', 'max:130'],
                'email' => ['string', 'email', 'max:255', 'unique:users'],
                'password' => ['string', 'min:6', 'confirmed'],
            ]);
            if($validator->fails()){
                return response()->json([
                    'errors' => [
                        'status' => 400,
                        'message' => 'Błąd podczas aktualizacji danych użytkownika',
                        'details' => $validator->errors()
                    ]
                ], 400);
            }

            if($data = User::find($id)) {

                $request->has('name')?
                    $data->name = request('name'):false;

                $request->has('lastName')?
                    $data->lastName = request('lastName'):false;

                $request->has('lastName')?
                    $data->lastName = request('lastName'):false;

                $request->has('role')?
                    $data->role = request('role'):false;

                $request->has('age')?
                    $data->age = request('age'):false;

                $request->has('email')?
                    $data->email = request('email'):false;

                $request->has('password')?
                    $data->password = Hash::make(request('password')):false;

                if($data->save()) {
                    return response()->json([
                        'status' => 200,
                        'message' => 'Dane użytkownika pomyślnie zaktualizowane',
                    ], 200);
                }
                else{
                    return response()->json([
                        'errors' => [
                            'status' => 400,
                            'message' => 'Błąd podczas aktualizacji danych użytkownika',
                        ]
                    ], 400);
                }
            }
            else{
                return response()->json([
                    'errors' => [
                        'status' => 404,
                        'message' => 'Nie ma użytkownika o danym ID',
                    ]
                ], 404);
            }
        }
        catch(\Exception $exception){
            return response()->json([
                'errors' => [
                    'status' => 400,
                    'message' => 'Użytkownik nie został utworzony',
                ]
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!is_numeric($id)){
            return response()->json([
                'errors' => [
                    'status' => 400,
                    'message' => 'Identyfikator użytkownika musi być liczbą',
                ]
            ], 400);
        }
        try {
            if($data = User::find($id)) {
                if($data->delete()){
                    return response()->json([
                        'status' => 200,
                        'message' => 'Użytkownik usunięty',
                    ], 200);
                }
                else {
                    return response()->json([
                        'errors' => [
                            'status' => 400,
                            'message' => 'Użytkownik nie usunięty',
                        ]
                    ], 400);
                }
            }

            if(!$data){
                return response()->json([
                    'errors' => [
                        'status' => 404,
                        'message' => 'Nie ma użytkownika o danym ID',
                    ]
                ], 404);
            }
        }
        catch(\Exception $exception){
            return response()->json([
                'errors' => [
                    'status' => 400,
                    'message' => 'Bad request',
                ]
            ], 200);
        }
        return response()->json(['data' => $data], 200);
    }
}
