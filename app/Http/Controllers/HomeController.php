<?php

namespace App\Http\Controllers;

use App\OAuth_client;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = OAuth_client::where('user_id', auth()->user()->id)->first();

        if(!$data){
            $data = OAuth_client::create([
                'user_id' => auth()->user()->id,
                'name' => auth()->user()->name,
                'secret' => hash('sha256', auth()->user()->id . time()),
                'redirect' => '',
                'revoked' => 0,
            ]);
        }
        return view('home', ['data'=>$data]);
    }
}
