<?php

namespace App\Http\Controllers;

use App\OAuth_client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthApiController extends Controller
{
    public function authApi(Request $request){

        //validation
        $validator = Validator::make($request->all(), [
            'client_id' => 'required',
            'client_secret' => 'required',
        ]);

        //validation - fails
        if ($validator->fails()) {
            return response()->json(['errors' => [
                'status' => 400,
                'message' => 'brakujące pola: client_id albo client_secret'
            ]]);
        }


        //check id
        if($data = OAuth_client::find(request('client_id'))){
            //check secret
            if($data->secret != request('client_secret')){
                return response()->json(['errors' => [
                    'status' => 401,
                    'message' => 'sprawdź poprawność wprowadzonych danych: client_id ta client_secret',
                ]]);
            }
        }
        else{
            return response()->json(['errors' => [
                'status' => 400,
                'message' => 'sprawdź poprawność wprowadzonych danych: client_id ta client_secret'
            ]]);
        }

        //create token
        $header = [
            'typ' => 'JWT',
            'alg' => 'sha256'
        ];
        $payload = [
            'iss' => 'Master Romio',
            'sub' => 'auth',
            'id'=> $data->id
        ];

        $signature = hash_hmac(env('APP_ALG'), base64_encode(json_encode([$data->id, $data->secret, $data->user_id])), env('APP_KEY'));

        $token = base64_encode(json_encode($header)) . '.' . base64_encode(json_encode($payload)) . '.' . $signature;

        return response()->json([
            'status' => 'success',
            'token' => $token,
        ], 200);

    }
}
