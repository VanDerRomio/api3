@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <h4><span>client_id</span>: <b>{{ $data->id }}</b></h4>
                    <h4><span>client_secret</span>: <b>{{ $data->secret }}</b></h4>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
