<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css\my_main.css') }}">
</head>
<body>
<div class="container m-t-05">
    <div class="card m-b-30">
        <div class="card-header">
            Informacje ogólne
        </div>
        <div class="card-body">
            <h5 class="card-title">Do korzystania z API wymaga:</h5>

            <a href="#register">
                <div class="alert alert-primary" role="alert">
                    Zarejestrować się na stronie i uzyskać <b>id</b> i <b>secret</b> aplikacji
                </div>
            </a>

            <a href="#auth">
                <div class="alert alert-secondary" role="alert">
                    Uzyskaj wygenerowany token i wysyłaj go w każdym żądaniu do Serwera API
                </div>
            </a>

            <a href="#rest">
                <div class="alert alert-success" role="alert">
                    W zależności od potrzeb użyj odpowiednich zapytań o odpowiednich parametrach
                </div>
            </a>
        </div>
    </div>

    <div id="register" class="card border-primary mb-3">
        <div class="card-header bgc-blue-50">Rejestracja</div>
        <div class="card-body">
            <p>Zarejestruj się na stronie: <a href="{{ env('APP_URL') }}/register" class="">{{ env('APP_URL') }}/register</a></p>
            <p>Jeśli już zarejestrowany, to zaloguj się : <a href="{{ env('APP_URL') }}/login" class="">{{ env('APP_URL') }}/login</a></p>
            <p>Po tym przejdź w link: <a href="{{ env('APP_URL') }}/home" class="">{{ env('APP_URL') }}/home</a></p>
            <p>Tutaj będą generowane <b>client_id</b> i <b>client_secret</b></p>
        </div>
    </div>

    <div id="auth" class="card border-secondary mb-3">
        <div class="card-header bgc-grey-50">Pobieranie tokenu</div>
        <div class="card-body">
            <p>Aby uzyskać token trzeba wysłać <b>client_id</b> i <b>client_secret</b> na adres:</p>
            <div class="alert alert-warning" role="alert">
                <b>POST</b> {{ env('APP_URL') }}/api/v3/<b>authorize</b>
            </div>

            <p>Dostaniesz odpowiedzi w formacie JSON:</p>
            <div class="alert alert-secondary" role="alert">
                <pre class="color-pink">
                    {
                        "status": "success",
                        "token": "eyJ0eXAiOiJKV1QiLCJhbGc.eyJpc3MiOiJNYXN0ZXIgUm9ta.47751a4c4b8145e75ed63ed"
                    }
                </pre>
            </div>
            <blockquote class="bshadow p-05 flex-row aitems-center fw-bold brr-4">
                <i class="fa fa-info fa-2x m-r-05" aria-hidden="true"></i>
                <span>Wszystkie odpowiedzi zarówno udane jak i o błędach, będziesz otrzymywać zawsze w formacie JSON</span>
            </blockquote>

            <blockquote class="bshadow p-05 flex-row aitems-center fw-bold brr-4">
                <i class="fa fa-info fa-2x m-r-05" aria-hidden="true"></i>
                <span>Ten token musi zostać przesłany na każde żądanie w nagłówku lub na końcu żądania</span>
            </blockquote>
        </div>
    </div>

    <div id="rest" class="card border-success mb-3">
        <div class="card-header bgc-green-50">Korzystanie z API</div>
        <div class="card-body">
            <div id="accordion">
                <!-- GET users -->
                <h3><b>GET</b> {{ env('APP_URL') }}/api/v3/users</h3>
                <div>
                    <p>Wyświetla dane wszystkich użytkowników:</p>
                    <div class="alert alert-warning" role="alert">
                        <b>GET</b> {{ env('APP_URL') }}/api/v3/<b>users</b>
                    </div>

                    <p>Dostaniesz odpowiedzi w formacie JSON:</p>
                    <div class="alert alert-secondary" role="alert">
                        <pre class="color-pink">
 [
     {
         "current_page": 1,
         "data": [
             {
                 "id": 1,
                 "name": "Romio",
                 "lastName": null,
                 "role": null,
                 "age": null,
                 "email": "master1988roma@gmail.com",
                 "password": "$2y$10$DJX7GtYxVHM5HbOtbeiAi.zm5.O6qWHn4zndrJ8FiPrihJLN8g48e",
                 "remember_token": "ZVXU5Ld4oBnjbCrEP6J4JNtv0WRnWcK14EEz17o1fQMpniYrYylwzzXushwx",
                 "created_at": "2019-01-18 18:13:45",
                 "updated_at": "2019-01-18 18:13:45"
             },
             {
                 "id": 2,
                 "name": "Dima",
                 "lastName": null,
                 "role": null,
                 "age": null,
                 "email": "dima@gmail.com",
                 "password": "$2y$10$gfskUJmcruoaydQsw9QRGOI2mgODmrsok8o5M0h894tbCH7bHSV4i",
                 "remember_token": "aWJjTyOA0fZjGVf8QDenScrlu3mHKp98OXOXfC3BVzV5eDC4iJ4AdfdI2uUE",
                 "created_at": "2019-01-19 20:22:21",
                 "updated_at": "2019-01-19 20:22:21"
             },
             {
                 "id": 3,
                 "name": "Alona",
                 "lastName": null,
                 "role": null,
                 "age": null,
                 "email": "alona@gmail.com",
                 "password": "$2y$10$/vZqocTrw/CcGkq/92I1le7hxCqATngzYdnC4o28QdgAHaspRaVIi",
                 "remember_token": "HbmXZoFCaazaardmDY8IMzreM8KfcbXbM0CZRi14lRmx9r5zbouvLP9n4COR",
                 "created_at": "2019-01-19 20:22:59",
                 "updated_at": "2019-01-19 20:22:59"
             }
         ],
         "first_page_url": "{{ env('APP_URL') }}/api/v3/users?page=1",
         "from": 1,
         "last_page": 1,
         "last_page_url": "{{ env('APP_URL') }}/api/v3/users?page=1",
         "next_page_url": null,
         "path": "{{ env('APP_URL') }}/api/v3/users",
         "per_page": 5,
         "prev_page_url": null,
         "to": 3,
         "total": 3
     }
 ]
                                </pre>
                    </div>

                    <p>Obecne są również następujące dane:</p>
                    <div class="alert alert-secondary" role="alert">
                        <p><span class="color-pink">current_page</span> - numer bieżącej strony</p>
                        <p><span class="color-pink">first_page_url</span> - url pierwszej strony</p>
                        <p><span class="color-pink">next_page_url</span> - url następnej strony</p>
                        <p><span class="color-pink">prev_page_url</span> - url poprzedniej strony</p>
                        <p><span class="color-pink">last_page_url</span> - url ostatniej strony</p>
                        <p><span class="color-pink">from</span> - liczba użytkowników, od których zaczyna się wyjście</p>
                        <p><span class="color-pink">last_page</span> - numer ostatniej strony</p>
                        <p><span class="color-pink">path</span> - adres</p>
                        <p><span class="color-pink">per_page</span> - ilość użytkowników do wydruku</p>
                        <p><span class="color-pink">to</span> - liczba użytkowników, do których następuje wycofanie</p>
                        <p><span class="color-pink">total</span> - liczba użytkowników</p>
                    </div>
                </div>

                <!-- GET users/id -->
                <h3><b>GET</b> {{ env('APP_URL') }}/api/v3/users/{id}</h3>
                <div>
                    <p>Wyświetla dane użytkownika z określonym identyfikatorem:</p>
                    <div class="alert alert-warning" role="alert">
                        <b>GET</b> {{ env('APP_URL') }}/api/v3/<b>users/{id}</b>
                    </div>

                    <p>Dostaniesz odpowiedzi w formacie JSON:</p>
                    <div class="alert alert-secondary" role="alert">
                        <pre class="color-pink">
 {
    "data": {
        "id": 1,
        "name": "Romio",
        "lastName": null,
        "role": null,
        "age": null,
        "email": "master1988roma@gmail.com",
        "password": "$2y$10$DJX7GtYxVHM5HbOtbeiAi.zm5.O6qWHn4zndrJ8FiPrihJLN8g48e",
        "remember_token": "ZVXU5Ld4oBnjbCrEP6J4JNtv0WRnWcK14EEz17o1fQMpniYrYylwzzXushwx",
        "created_at": "2019-01-18 18:13:45",
        "updated_at": "2019-01-18 18:13:45"
    }
}
                                </pre>
                    </div>
                </div>

                <!-- POST users -->
                <h3><b>POST</b> {{ env('APP_URL') }}/api/v3/users</h3>
                <div>
                    <p>Dodawanie użytkownika:</p>
                    <div class="alert alert-primary" role="alert">
                        <b>POST</b> {{ env('APP_URL') }}/api/v3/<b>users</b>
                    </div>

                    <p>Parameters:</p>
                    <div class="alert alert-secondary" role="alert">
                        <p><span class="color-pink">name</span> - required, string, max=255</p>
                        <p><span class="color-pink">lastName</span> - required, string, max=255</p>
                        <p><span class="color-pink">role</span> - required, string, max=255</p>
                        <p><span class="color-pink">age</span> - required, integer, max=130</p>
                        <p><span class="color-pink">email</span> - required, string, email, max=255, unique(users)</p>
                        <p><span class="color-pink">password</span> - required, string, min=6, confirmed</p>
                        <p><span class="color-pink">password_confirmation</span> - required, string, min=6</p>
                    </div>

                    <p>Dostaniesz odpowiedzi w formacie JSON:</p>
                    <p></p>
                    <div class="alert alert-secondary" role="alert">
                        <pre class="color-pink">
 {
    "status": "success",
    "message": "Użytkownik został pomyślnie utworzony"
 }
                                </pre>
                    </div>

                    <div class="alert alert-secondary" role="alert">
                        <pre class="color-pink">
 {
    "errors": {
        "status": 400,
        "message": "Użytkownik nie został utworzony",
        "details": {
            "name": [
                "The name field is required."
            ],
            "email": [
                "The email has already been taken."
            ]
        }
    }
 }
                                </pre>
                    </div>
                </div>

                <!-- PUT users -->
                <h3><b>PUT</b> {{ env('APP_URL') }}/api/v3/users/{id}</h3>
                <div>
                    <p>Aktualizacja danych użytkownika:</p>
                    <div class="alert alert-primary" role="alert">
                        <b>PUT</b> {{ env('APP_URL') }}/api/v3/<b>users/{id}</b>
                    </div>

                    <p>Parameters:</p>
                    <div class="alert alert-secondary" role="alert">
                        <p><span class="color-pink">name</span> - string, max=255</p>
                        <p><span class="color-pink">lastName</span> - string, max=255</p>
                        <p><span class="color-pink">role</span> - string, max=255</p>
                        <p><span class="color-pink">age</span> - integer, max=130</p>
                        <p><span class="color-pink">email</span> - string, email, max=255, unique(users)</p>
                        <p><span class="color-pink">password</span> - string, min=6, confirmed</p>
                        <p><span class="color-pink">password_confirmation</span> - string, min=6</p>
                    </div>

                    <p>Dostaniesz odpowiedzi w formacie JSON:</p>
                    <p></p>
                    <div class="alert alert-secondary" role="alert">
                        <pre class="color-pink">
{
    "status": 200,
    "message": "Dane użytkownika pomyślnie zaktualizowane"
}
                                </pre>
                    </div>

                    <div class="alert alert-secondary" role="alert">
                        <pre class="color-pink">
{
    "errors": {
        "status": 400,
        "message": "Błąd podczas aktualizacji danych użytkownika",
        "details": {
            "age": [
                "The age may not be greater than 130."
            ]
        }
    }
}
                                </pre>
                    </div>
                </div>

                <!-- DELETE users/id -->
                <h3><b>DELETE</b> {{ env('APP_URL') }}/api/v3/users/{id}</h3>
                <div>
                    <p>Usuwanie użytkownika z określonym identyfikatorem:</p>
                    <div class="alert alert-warning" role="alert">
                        <b>DELETE</b> {{ env('APP_URL') }}/api/v3/<b>users/{id}</b>
                    </div>

                    <p>Dostaniesz odpowiedzi w formacie JSON:</p>
                    <div class="alert alert-secondary" role="alert">
                        <pre class="color-pink">
{
        "status": 200,
        "message": "Użytkownik usunięty"
}
                                </pre>
                    </div>

                    <div class="alert alert-secondary" role="alert">
                        <pre class="color-pink">
{
    "errors": {
        "status": 404,
        "message": "nie ma użytkownika o danym ID"
    }
}
                                </pre>
                    </div>
                </div>

                <!-- sortowanie danych -->
                <h3><b>Sortowanie danych</b></h3>
                <div>
                    <p>Segregujemy użytkowników według nazwy:</p>
                    <div class="alert alert-warning" role="alert">
                        <b>GET</b> {{ env('APP_URL') }}/api/v3/<b>users?sort=name</b>
                    </div>

                    <p>Dostaniesz odpowiedzi w formacie JSON:</p>
                    <div class="alert alert-secondary" role="alert">
                        <pre class="color-pink">
[
    {
        "current_page": 1,
        "data": [
            {
                "id": 3,
                "name": "Alona",
                "lastName": null,
                "role": null,
                "age": null,
                "email": "alona@gmail.com",
                "password": "$2y$10$/vZqocTrw/CcGkq/92I1le7hxCqATngzYdnC4o28QdgAHaspRaVIi",
                "remember_token": "HbmXZoFCaazaardmDY8IMzreM8KfcbXbM0CZRi14lRmx9r5zbouvLP9n4COR",
                "created_at": "2019-01-19 20:22:59",
                "updated_at": "2019-01-19 20:22:59"
            },
            {
                "id": 2,
                "name": "Dima",
                "lastName": null,
                "role": null,
                "age": null,
                "email": "dima@gmail.com",
                "password": "$2y$10$gfskUJmcruoaydQsw9QRGOI2mgODmrsok8o5M0h894tbCH7bHSV4i",
                "remember_token": "aWJjTyOA0fZjGVf8QDenScrlu3mHKp98OXOXfC3BVzV5eDC4iJ4AdfdI2uUE",
                "created_at": "2019-01-19 20:22:21",
                "updated_at": "2019-01-19 20:22:21"
            },
            {
                "id": 5,
                "name": "Nikolaj",
                "lastName": "N.",
                "role": "admin",
                "age": 33,
                "email": "nikolaj@gmail.com",
                "password": "$2y$10$Ph4IR4nZDSOIkk.L/kMOhe26GUEEDpyiEj8TR11ZXjJkoR7PS.yQa",
                "remember_token": null,
                "created_at": "2019-01-21 14:48:58",
                "updated_at": "2019-01-21 14:48:58"
            }
        ],
        "first_page_url": "http://api3.pl/api/v3/users?page=1",
        "from": 1,
        "last_page": 2,
        "last_page_url": "http://api3.pl/api/v3/users?page=2",
        "next_page_url": "http://api3.pl/api/v3/users?page=2",
        "path": "http://api3.pl/api/v3/users",
        "per_page": 3,
        "prev_page_url": null,
        "to": 3,
        "total": 4
    }
]
                                </pre>
                    </div>

                    <div class="alert alert-warning" role="alert">
                        <b>GET</b> {{ env('APP_URL') }}/api/v3/<b>users?sort=name&orderby=desc</b>
                    </div>

                    <p>Dostaniesz odpowiedzi w formacie JSON:</p>
                    <div class="alert alert-secondary" role="alert">
                        <pre class="color-pink">
[
    {
        "current_page": 1,
        "data": [
            {
                "id": 1,
                "name": "Romio",
                "lastName": null,
                "role": null,
                "age": null,
                "email": "master1988roma@gmail.com",
                "password": "$2y$10$DJX7GtYxVHM5HbOtbeiAi.zm5.O6qWHn4zndrJ8FiPrihJLN8g48e",
                "remember_token": "ZVXU5Ld4oBnjbCrEP6J4JNtv0WRnWcK14EEz17o1fQMpniYrYylwzzXushwx",
                "created_at": "2019-01-18 18:13:45",
                "updated_at": "2019-01-18 18:13:45"
            },
            {
                "id": 5,
                "name": "Nikolaj",
                "lastName": "N.",
                "role": "admin",
                "age": 33,
                "email": "nikolaj@gmail.com",
                "password": "$2y$10$Ph4IR4nZDSOIkk.L/kMOhe26GUEEDpyiEj8TR11ZXjJkoR7PS.yQa",
                "remember_token": null,
                "created_at": "2019-01-21 14:48:58",
                "updated_at": "2019-01-21 14:48:58"
            },
            {
                "id": 2,
                "name": "Dima",
                "lastName": null,
                "role": null,
                "age": null,
                "email": "dima@gmail.com",
                "password": "$2y$10$gfskUJmcruoaydQsw9QRGOI2mgODmrsok8o5M0h894tbCH7bHSV4i",
                "remember_token": "aWJjTyOA0fZjGVf8QDenScrlu3mHKp98OXOXfC3BVzV5eDC4iJ4AdfdI2uUE",
                "created_at": "2019-01-19 20:22:21",
                "updated_at": "2019-01-19 20:22:21"
            }
        ],
        "first_page_url": "http://api3.pl/api/v3/users?page=1",
        "from": 1,
        "last_page": 2,
        "last_page_url": "http://api3.pl/api/v3/users?page=2",
        "next_page_url": "http://api3.pl/api/v3/users?page=2",
        "path": "http://api3.pl/api/v3/users",
        "per_page": 3,
        "prev_page_url": null,
        "to": 3,
        "total": 4
    }
]
                                </pre>
                    </div>
                </div>

                <!-- wycofujemy tylko określone pola użytkowników -->
                <h3><b>Wycofujemy tylko określone pola użytkowników</b></h3>
                <div>
                    <p>Wycofujemy tylko określone pola użytkowników:</p>
                    <div class="alert alert-warning" role="alert">
                        <b>GET</b> {{ env('APP_URL') }}/api/v3/<b>users?select=id,name,email</b>
                    </div>

                    <p>Dostaniesz odpowiedzi w formacie JSON:</p>
                    <div class="alert alert-secondary" role="alert">
                        <pre class="color-pink">
[
    {
        "current_page": 1,
        "data": [
            {
                "id": 1,
                "name": "Romio",
                "email": "master1988roma@gmail.com"
            },
            {
                "id": 2,
                "name": "Dima",
                "email": "dima@gmail.com"
            },
            {
                "id": 3,
                "name": "Alona",
                "email": "alona@gmail.com"
            }
        ],
        "first_page_url": "http://api3.pl/api/v3/users?page=1",
        "from": 1,
        "last_page": 2,
        "last_page_url": "http://api3.pl/api/v3/users?page=2",
        "next_page_url": "http://api3.pl/api/v3/users?page=2",
        "path": "http://api3.pl/api/v3/users",
        "per_page": 3,
        "prev_page_url": null,
        "to": 3,
        "total": 4
    }
]
                                </pre>
                    </div>

                    <div class="alert alert-warning" role="alert">
                        <b>GET</b> {{ env('APP_URL') }}/api/v3/<b>users/2?select=id,name,email</b>
                    </div>

                    <p>Dostaniesz odpowiedzi w formacie JSON:</p>
                    <div class="alert alert-secondary" role="alert">
                        <pre class="color-pink">
{
    "data": {
        "id": 2,
        "name": "Dima",
        "email": "dima@gmail.com"
    }
}
                                </pre>
                    </div>

                    <div class="alert alert-warning" role="alert">
                        <b>GET</b> {{ env('APP_URL') }}/api/v3/<b>users?select=id,title</b>
                    </div>

                    <p>Dostaniesz odpowiedzi w formacie JSON:</p>
                    <div class="alert alert-secondary" role="alert">
                        <pre class="color-pink">
{
    "errors": {
        "status": 400,
        "message": "użytkownik nie ma atrybutu: title"
    }
}
                                </pre>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#accordion" ).accordion({
            collapsible: true,
            active: false,
            heightStyle: "content",
            autoHeight: false,
        });
    } );
</script>
</body>
</html>
