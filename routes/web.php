<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dokumentacja', function () {
    return view('dokumentacja');
})->name('dokumentacja');



Route::prefix('api/v3')->group(function(){
    Route::post('authorize', 'AuthApiController@authApi')->name('authorize');

    Route::middleware('check-token')->group(function(){
        Route::resource('users', 'Api\UserController');
    });
});






Route::get('home', 'HomeController@index')->name('home');

Auth::routes();
